'use strict';

var userID = 'UzzielMilian';
var apiKey = 'h4QZLm74MMvtbFrbv9RUknTLwl2PUzNJ';

angular.module('uzzielPwApp')
  .factory('behanceUserData', ['$q', '$http',
    function($q, $http) {
      var service = {
        getUser: function() {
          var d = $q.defer();
          $http({
            method: 'GET',
            url: 'https://www.behance.net/v2/users/' + userID + '?api_key=' + apiKey
          }).success(function(data) {
            console.log(data);
            d.resolve(data);
          }).error(function(reason) {
            console.log(reason);
            d.reject(reason);
          });
          return d.promise;
        },
        getProjects: function() {
          var d = $q.defer();
          $http({
            method: 'GET',
            url:'https://www.behance.net/v2/users/' + userID + '/projects?api_key=' + apiKey
         
          })
          .success(function(data){
            console.log(data);
            d.resolve(data);

          })
          .error(function(reason){
            console.log(reason);
            d.reject(reason);
          });
          return d.promise;
        },
        getProjectDetail: function(projectID){
          var d = $q.defer();
          $http({
            method:'GET',
            url: 'http://www.behance.net/v2/projects/' + projectID + '?api_key=' + apiKey
          })
          .success(function(data){
            console.log(data);
            d.resolve(data);
          })
          .error(function(reason){
            console.log(reason);
            d.reject(reason);
          });
          return d.promise;
        }
      };
      return service;

    }
  ]);
