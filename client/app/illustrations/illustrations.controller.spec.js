'use strict';

describe('Controller: IllustrationsCtrl', function () {

  // load the controller's module
  beforeEach(module('uzzielPwApp'));

  var IllustrationsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IllustrationsCtrl = $controller('IllustrationsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
