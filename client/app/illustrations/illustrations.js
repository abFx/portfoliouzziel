'use strict';

angular.module('uzzielPwApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/illustrations', {
        templateUrl: 'app/illustrations/illustrations.html',
        controller: 'IllustrationsCtrl'
      });
  });

