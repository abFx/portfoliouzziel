'use strict';



angular.module('uzzielPwApp')
  .controller('IllustrationsCtrl', function ($scope, $http, $location, behanceUserData) {
    $scope.awesomeThings = [];

    behanceUserData.getProjects().then(
        function(data){
          if(data.http_code === 200)
            $scope.projects = data.projects;
        },
        function(error){
          alert('Oops error');
        }
        );
    
    $scope.mainGoTo = function(name, projectId){
      $location.path('/projects/' + name).search({id: projectId});
    };

  });

