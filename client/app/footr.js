'use strict';

angular.module('uzzielPwApp')
	.controller('FooterCtrl', ['$scope', function($scope){
		$scope.footer = [
			{url:'', title: 'Linkedin'},
			{url:'', title: 'Behance'},
			{url: '', title: 'Pinterest'}
		];

	}]);
