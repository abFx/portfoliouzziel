'use strict';


angular.module('uzzielPwApp')
  .controller('MainCtrl', function ($scope, $http, $location, behanceUserData) {
    $scope.awesomeThings = [];

    $http.get('/api/things').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
    });

    behanceUserData.getProjects().then(
        function(data){
          if(data.http_code === 200)
            $scope.projects = data.projects;
        },
        function(error){
          alert('Oops error');
        }
        );

    //go to project detail
    $scope.mainGoTo = function(name, projectId){
      $location.path('/projects/' + name).search({id: projectId});
    };
});

