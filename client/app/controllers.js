'use strict';

angular.module('uzzielPwApp')
	.controller('NavCtrl', ['$scope', '$location', function($scope, $location){
		$scope.nav = [
			{path:'/', title: 'UI/Interaction'},
			{path:'/illustrations', title: 'Illustrations'},
			{path: '/about', title: 'About'}
		];
		$scope.isActive = function(item) {
			if (item.path === $location.path()){
				return true;
			}
			return false;
		};

	}]);

