'use strict';

angular.module('uzzielPwApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/projects/:id', {
        templateUrl: 'app/detail/detail.html',
        controller: 'DetailCtrl'
      });
  });
