'use strict';

var uzzielPwApp = angular.module('uzzielPwApp');
uzzielPwApp.controller('DetailCtrl', function ($scope, $http, $location, behanceUserData) {
    $scope.awesomeThings = [];

    $http.get('/api/things').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
    });
    var query = $location.search();

    behanceUserData.getProjectDetail(query['id'])
                    .then(function(data){
                      if(data.http_code === 200)
                        $scope.project = data.project;
                    },
                    function(error){
                      alert('Oops error');
                    }
                    );
    $scope.goTo = function(url){
      $location.path(url);
    };
                  
  });

uzzielPwApp.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});